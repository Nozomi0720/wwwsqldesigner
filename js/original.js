const base64ToBlob = uri => {
    const parse = uri.slice(5).split(/[,;]/);
    const binStr = atob(parse.pop());
    const l = binStr.length;
    const array = new Uint8Array(l);

    for (let i = 0; i < l; i++) {
        array[i] = binStr.charCodeAt(i);
    }

    return new Blob([array], {type: parse[0]});
    }
    function div2canvas() {
        html2canvas(document.getElementById("area")).then(canvas => {
            const url = URL.createObjectURL(base64ToBlob(canvas.toDataURL()));
            downloadlink = document.getElementById("imagedownload");
            downloadlink.href = url;
            downloadlink.download = "erImage.png";
            downloadlink.click();
            URL.revokeObjectURL(url); // オブジェクトURLを開放
        });
    }